function distance(first, second) {
	//Validare:
	if (!(Array.isArray(first) && Array.isArray(second)))
		throw new Error("InvalidType");

	//Prelucrare date:
	let first_prelucrat = first.filter((v, i) => first.indexOf(v) === i).sort();
	let second_prelucrat = second.filter((v, i) => second.indexOf(v) === i).sort();

	//Calcul distanta:
	distance = 0;
	for (let i = 0; i < first_prelucrat.length; i++)
		second_prelucrat.indexOf(first_prelucrat[i])===-1? distance++ : distance;
	for (let i = 0; i < second_prelucrat.length; i++)
		first_prelucrat.indexOf(second_prelucrat[i])===-1? distance++ : distance;
	return distance;
}


module.exports.distance = distance